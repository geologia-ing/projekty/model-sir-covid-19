#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import numpy as np
import pandas as pd
from scipy.integrate import solve_ivp
from scipy.optimize import minimize
import matplotlib.pyplot as plt
from datetime import timedelta, datetime
import tkinter as tk


def loss(point, data, recovered, s_0, i_0, r_0):
    size = len(data)
    beta, gamma = point

    def SIR(t, y):
        S = y[0]
        I = y[1]
        R = y[2]
        N = S + I + R
        return [-beta * S * I / N, beta * S * I / N - gamma * I, gamma * I]

    solution = solve_ivp(SIR, [0, size], [s_0, i_0, r_0], t_eval=np.arange(0, size, 1), vectorized=True)
    l1 = np.sqrt(np.mean((solution.y[1] - data) ** 2))
    l2 = np.sqrt(np.mean((solution.y[2] - recovered) ** 2))
    alpha = 0.1
    return alpha * l1 + (1 - alpha) * l2


def load_csv_data(country, type, start_date, end_date):
    df = pd.read_csv('data/' + type + '.csv')
    country_df = df[df['Country/Region'] == country]
    return country_df.iloc[0].loc[start_date:end_date]


def load_data(country, start_date, end_date):
    recovered = load_csv_data(country, 'recovered', start_date, end_date)
    death = load_csv_data(country, 'deaths', start_date, end_date)
    data = (load_csv_data(country, 'confirmed', start_date, end_date) - recovered - death)
    return recovered, death, data


def extend_index(index, new_size):
    values = index.values
    current = datetime.strptime(index[-1], '%m/%d/%y')
    while len(values) < new_size:
        current = current + timedelta(days=1)
        values = np.append(values, datetime.strftime(current, '%m/%d/%y'))
    return values


def predict(beta, gamma, data, recovered, death, country, s_0, i_0, r_0, predict_range):
    new_index = extend_index(data.index, predict_range)
    size = len(new_index)

    def SIR(t, y):
        S = y[0]
        I = y[1]
        R = y[2]
        N = S + I + R
        return [-beta * S * I / N, beta * S * I / N - gamma * I, gamma * I]

    extended_actual = np.concatenate((data.values, [None] * (size - len(data.values))))
    extended_recovered = np.concatenate((recovered.values, [None] * (size - len(recovered.values))))
    extended_death = np.concatenate((death.values, [None] * (size - len(death.values))))
    return new_index, extended_actual, extended_recovered, extended_death, solve_ivp(SIR, [0, size],
                                                                                     [s_0, i_0, r_0],
                                                                                     t_eval=np.arange(0, size, 1))


def print_data(country, extended_actual, extended_death, prediction, new_index):
    df = pd.DataFrame({'Zarazeni (z danych)': extended_actual,
                       'Zgony (z danych)': extended_death,
                       'Zarażeni (predykcja)': prediction.y[1],
                       },
                      index=new_index)

    fig, ax = plt.subplots(figsize=(15, 10))

    title = country + " wynik prognozowania modelu SIR"
    ax.set_title(title)
    ax.set_facecolor('xkcd:black')

    df.plot(ax=ax)

    file_title = title.replace(" ", "_")
    fig.savefig(f"{file_title}.png")



def run_ui(loss, start_date, end_date, predict_range, s_0, i_0, r_0):
    root = tk.Tk()
    root.title('Model epidemiologiczny SIR')

    canvas = tk.Canvas(root, width=400, height=300, relief='raised')
    canvas.pack()

    country_label_description = tk.Label(root, text='Podaj nazwę kraju jako źródła danych:')
    canvas.create_window(200, 100, window=country_label_description)

    country_edit_box = tk.Entry(root)
    canvas.create_window(200, 140, window=country_edit_box)

    output_var = tk.StringVar()
    output_label = tk.Label(root, textvariable = output_var )
    canvas.create_window(200, 210, window=output_label)

    def run_sir_model():
        try:
            country = country_edit_box.get()
            # wczytanie danych z 3 plików CSV
            recovered, death, data = load_data(country, start_date, end_date)

            # wyliczenie minium z funkcji loss
            minimum = minimize(loss, [0.001, 0.001], args=(data, recovered, s_0, i_0, r_0), method='L-BFGS-B',
                               bounds=[(0.00000001, 0.4), (0.00000001, 0.4)])

            beta, gamma = minimum.x

            # zastosowanie modelu SIR
            new_index, extended_actual, extended_recovered, extended_death, prediction = predict(beta, gamma, data,
                                                                                                 recovered,
                                                                                                 death, country, s_0, i_0,
                                                                                                 r_0,
                                                                                                 predict_range)
            # wydrukowanie 3 serii danych
            print_data(country, extended_actual, extended_death, prediction, new_index)

            output_var.set('Wygenerowano wykres dla kraju: ' + country)
        except:
            output_var.set('Proszę użyć poprawnej, angielskiej nazwy kraju')


    button1 = tk.Button(text='Użyj modelu', command=run_sir_model, bg='brown', fg='black')
    canvas.create_window(200, 180, window=button1)

    root.mainloop()


if __name__ == '__main__':
    start_date = "3/1/20"
    end_date = "5/1/20"
    predict_range = 100
    s_0 = 100000
    i_0 = 2
    r_0 = 10

    run_ui(loss, start_date, end_date, predict_range, s_0, i_0, r_0)

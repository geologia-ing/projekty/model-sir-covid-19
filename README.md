# Model epidemiologiczny SIR

Plik SIR.py zawiera skrypt demonstrujący zasadę wyliczenia modelu epidemiologicznego SIR. Model ten swoją nazwę bierze od głównych czynników, z których korzysta, czyli liczby osób podatnych na zarażenie (Susceptible), zainfekowanych (Infective) i wyzdrowiałych (Removed). Osoby podatne na zagrożenie to grupa ludzi, która narażona jest na kontakt z ludźmi zainfekowanymi. Grupa zarażonych reprezentowana jest przez ludzi aktywnie zarażonych, którzy chorują w danym momencie na wirusa. Zarażają oni grupę podatnych. Trzecia grupa to grupa wyzdrowiałych, w której zakłada się, że nabywa ona odporność na patogen lub zostają całkowicie odizolowani od społeczeństwa i nie mogą na nowo się zarazić.

Funkcje odpowiedzialne za samo wyliczenie prognozy są wzorowane na kawałkach kodu z artykułu https://www.lewuathe.com/covid-19-dynamics-with-sir-model.html 

W repozytorium oprócz skryptu SIR.py znajdują się źródła danych, które zawierają informację o liczbie osób które: aktualnie chorują, zmarły i wyzdrowiały na COVID w kontekście danego kraju w przedziale dniowym. Dane pochodzą z repozytorium https://github.com/bumbeishvili/covid19-daily-data, które było aktualizowane codziennie od stycznia 2020 do kwietania 2022. Dane przedstawione są w postaci trzech plików CSV.

W programie do rozwiązania problemu zostały użyte funkcja minimize z pakietu scipy, która pozwala na wyliczenie minimum dla podanej funkcji oraz funkcja solve_ivp, która pozwala na rozwiązania równania różniczkowego. Do samego załadowania danych z plików CSV zdecydowałam się użyć pakietu Panda, ze względu na jego wbudowaną funkcję read_csv.  


## Uruchomienie programu
Aby uruchominić program wystarczy użyć komendy 
```sh
python SIR.py
```

Do poprawnego działania programu potrzebne jest posiadanie następujących bibliotek:
- pandas
- scipy
- matplotlib.pyplot
- datetime
- numpy
- tkinter

Program zawiera dane ustawione na sztywno w funkcji main, które można zmienić i uzyskać predykcje dla innych parametrów. Parametrami tymi są:
start_date - data początkowa analizy
end_date - data końcowa analizy
predict_range - liczba dni "do przodu", które program ma wymodelować

Parametrem, który możemy zmienić w sposób dynamiczny jest kraj, którego dotyczy analiza. Po uruchomieniu programu użytkownikowi pojawi się okienko z możliwością wpisania kraju oraz przyciskiem do wygenerowania analizy. 

W repozytorium znajduje się także przykładowy rezultat wywołania programu z domyślnymi parametrami w postaci pliku Poland_wynik_prognozowania_modelu_SIR.png.


## Działanie programu

<div align="center">
    <img src="/sample/program-screenshot.png" width="400px"</img> 
</div>

## Przykładowy rezultat
<div align="center">
    <img src="/Poland_wynik_prognozowania_modelu_SIR.png" width="400px"</img> 
</div>
